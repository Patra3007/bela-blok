package com.belablok.BelaBlokApp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>  {
    private static final String TAG = "RecyclerViewAdapter";

    private ArrayList<String> mWePoints = new ArrayList<>();
    private ArrayList<String> mYouPoints = new ArrayList<>();
    private Context mContext;
    private OnItemClickListener Listener;


    public interface OnItemClickListener{
        void onDeleteClick(int position);
        void onEditClick(int position,Context Context1);
    }
    public void setOnItemClickListener(OnItemClickListener listener){
        Listener = listener;
    }

    public RecyclerViewAdapter(ArrayList<String> WePoints, ArrayList<String> YouPoints, Context Context) {
        this.mWePoints = WePoints;
        this.mYouPoints = YouPoints;
        this.mContext = Context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_one_part, parent,false)    ;
        ViewHolder holder = new ViewHolder(view, Listener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.we.setText(mWePoints.get(position));
        holder.you.setText(mYouPoints.get(position));

    }

    @Override
    public int getItemCount() {
       return mWePoints.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView we;
        TextView you;
        ConstraintLayout parentLayout;
        ImageView delete;
        ImageView update;
        public ViewHolder(@NonNull View itemView,OnItemClickListener listener) {
            super(itemView);
            we = itemView.findViewById(R.id.we);
            you = itemView.findViewById(R.id.you);
            delete = (ImageView) itemView.findViewById(R.id.image_delete);
            update = (ImageView) itemView.findViewById(R.id.edit);
            parentLayout = itemView.findViewById(R.id.parent_layout);

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onDeleteClick(position);
                        }
                    }
                }
            });
            update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onEditClick(position,mContext);
                        }
                    }
                }
            });


        }
    }
}



package com.belablok.BelaBlokApp;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;



public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    Window window;
    Dialog Shuffles;
    Dialog Reset;
    private RecyclerViewAdapter adapter;
    private Button input;
    private Button reset;
    private Button inputPlayer;
    private TextView resultsWe;
    private TextView resultsYou;
    private TextView scoreWe;
    private TextView scoreYou;

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);
        if(Build.VERSION.SDK_INT >=23 ){
            window=this.getWindow();
            window.setStatusBarColor(this.getResources().getColor(R.color.black));
        }
        initRecylerView();
        inicalize();
        Shuffles = new Dialog(this);
        Reset = new Dialog(this);
        set_on_click_listeners(this);
        boolean InputGame = getIntent().getBooleanExtra("EXTRA_INPUTYN", false);
        boolean Update = getIntent().getBooleanExtra("EXTRA_INPUTYN_CHANGE", false);
        if(Update){
            updateItem();
        }
        else if (InputGame) {
            insertItem();
        }
        updateScore();
    }

    private void updateScore() {
        check_for_winner(GlobalResults.mWePoints, GlobalResults.mYouPoints);
        setState(GlobalResults.mWePoints, GlobalResults.mYouPoints);
        scoreWe.setText(String.valueOf(GlobalResults.scoreWe));
        scoreYou.setText(String.valueOf(GlobalResults.scoreYou));
    }

    private void reset() {
        GlobalResults.scoreWe=0;
        GlobalResults.scoreYou=0;
        GlobalResults.names.clear();
        GlobalResults.shuffle=1;
        GlobalResults.mWePoints.clear();
        GlobalResults.mYouPoints.clear();
        GlobalResults.WePoints.clear();
        GlobalResults.YouPoints.clear();
        GlobalResults.WePoints.clear();
        GlobalResults.YouPoints.clear();
        GlobalResults.wecallings.clear();
        GlobalResults.youcallings.clear();
    }
    void set_on_click_listeners(Context this1){
        reset.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                showResetYesNo();
            }
        });
        if (Build.VERSION.SDK_INT >= 23) {
            window = this.getWindow();
            window.setStatusBarColor(this1.getResources().getColor(R.color.black));
        }
        input.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity2();
            }
        });
        inputPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity3();
            }
        });
    }
    private void initRecylerView() {
        RecyclerView recyclerView = findViewById(R.id.rvGames);
        this.adapter = new RecyclerViewAdapter(GlobalResults.WePoints, GlobalResults.YouPoints, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter.setOnItemClickListener(new RecyclerViewAdapter.OnItemClickListener() {
            public void onDeleteClick(int position) {
                if (GlobalResults.shuffle == 0) {
                    GlobalResults.shuffle = 3;
                } else {
                    GlobalResults.shuffle -= 1;
                }
                GlobalResults.mWePoints.remove(position);
                GlobalResults.mYouPoints.remove(position);
                GlobalResults.WePoints.remove(position);
                GlobalResults.YouPoints.remove(position);
                adapter.notifyItemRemoved(position);
                setState(GlobalResults.mWePoints, GlobalResults.mYouPoints);
            }

            @Override
            public void onEditClick(int position, Context Context1) {
                Intent intent1 = new Intent(Context1,Activity2.class);
                intent1.putExtra("EXTRA_UPDATE",true);
                intent1.putExtra("EXTRA_position",position);
                startActivity(intent1);
            }


        });
    }



    private void inicalize() {
        this.input = (Button) findViewById(R.id.input);
        this.reset = (Button) findViewById(R.id.reset);
        this.resultsWe = (TextView) findViewById(R.id.resultsWe);
        this.resultsYou = (TextView) findViewById(R.id.resultsYou);
        this.scoreWe = (TextView) findViewById(R.id.scoreWe);
        this.scoreYou = (TextView) findViewById(R.id.scoreYou);
        this.inputPlayer = (Button) findViewById(R.id.inputPlayer);
    }

    private void openActivity3() {
        Intent intent = new Intent(this, Activity3_1.class);
        startActivity(intent);
    }


    public void insertItem() {
        int PointsWe = getIntent().getIntExtra("EXTRA_POINTSWE", 0);
        int PointsYou = getIntent().getIntExtra("EXTRA_POINTSYOU", 0);
        GlobalResults.mWePoints.add(PointsWe);
        GlobalResults.mYouPoints.add(PointsYou);
        GlobalResults.WePoints.add(String.valueOf(PointsWe));
        GlobalResults.YouPoints.add(String.valueOf(PointsYou));
        adapter.notifyItemInserted(GlobalResults.mWePoints.size()+1);
        setState(GlobalResults.mWePoints, GlobalResults.mYouPoints);
    }
    public void updateItem(){
        int position = getIntent().getIntExtra("EXTRA_POSITION",0);
        int PointsWe =getIntent().getIntExtra("EXTRA_POINTSWE_CHANGE",0);
        int PointsYou =getIntent().getIntExtra("EXTRA_POINTSYOU_CHANGE",0);
        GlobalResults.mWePoints.set(position,PointsWe);
        GlobalResults.mYouPoints.set(position,PointsYou);
        GlobalResults.WePoints.set(position,String.valueOf(PointsWe));
        GlobalResults.YouPoints.set(position,String.valueOf(PointsYou));
        adapter.notifyItemChanged(position);
        setState(GlobalResults.mWePoints, GlobalResults.mYouPoints);
    }

    public void setState(ArrayList<Integer> WePoints1, ArrayList<Integer> YouPoints1) {
        Integer wePoint = calculate_score(WePoints1);
        Integer youPoint = calculate_score(YouPoints1);
        resultsWe.setText(String.valueOf(wePoint));
        resultsYou.setText(String.valueOf(youPoint));
    }

    public Integer calculate_score(ArrayList<Integer> Points1) {
        int points = 0;
        for (int i = 0; i < Points1.size(); i++)
            points += Points1.get(i);
        return points;
    }

    private void check_for_winner(ArrayList<Integer> WePoints, ArrayList<Integer> YouPoints) {
        int wePoint = calculate_score(WePoints);
        int youPoint = calculate_score(YouPoints);
        if (wePoint == youPoint && youPoint > 1000) {
        }
        else {
            if (wePoint >= 1001) {
                if (youPoint < 1001) {
                    clear();
                    showShuffles();
                    GlobalResults.scoreWe += 1;
                    displayToast("MI");
                } else {
                    if (wePoint > youPoint) {
                        clear();
                        showShuffles();
                        GlobalResults.scoreWe += 1;
                        displayToast("MI");
                    } else {
                        clear();
                        showShuffles();
                        GlobalResults.scoreYou += 1;
                        displayToast("VI");
                    }
                }
            } else if (youPoint >= 1001) {
                clear();
                showShuffles();
                GlobalResults.scoreYou += 1;
                displayToast("VI");
            }
        }
    }
    private void clear() {
        GlobalResults.mWePoints.clear();
        GlobalResults.mYouPoints.clear();
        GlobalResults.WePoints.clear();
        GlobalResults.YouPoints.clear();
        GlobalResults.shuffle=1;
    }

    public void showShuffles() {
        if (GlobalResults.names.isEmpty() || GlobalResults.names.get(0).isEmpty()) {
            GlobalResults.names.clear();
        }
        if (GlobalResults.names.isEmpty() == false) {
            Button player1;
            Button player2;
            Button player3;
            Button player4;
            Shuffles.setContentView(R.layout.popup);
            player1 = (Button) Shuffles.findViewById(R.id.player1);
            player2 = (Button) Shuffles.findViewById(R.id.player2);
            player3 = (Button) Shuffles.findViewById(R.id.player3);
            player4 = (Button) Shuffles.findViewById(R.id.player4);

            player1.setText(GlobalResults.names.get(0));
            player2.setText(GlobalResults.names.get(1));
            player3.setText(GlobalResults.names.get(2));
            player4.setText(GlobalResults.names.get(3));

            player1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GlobalResults.shuffle = 1;
                    Shuffles.dismiss();
                }
            });
            player2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GlobalResults.shuffle = 2;
                    Shuffles.dismiss();
                }
            });
            player3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GlobalResults.shuffle = 3;
                    Shuffles.dismiss();
                }
            });
            player4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GlobalResults.shuffle = 0;
                    Shuffles.dismiss();
                }
            });
        Shuffles.show();
        }
    }

    public void showResetYesNo() {
        Button yes;
        Button no;
        Reset.setContentView(R.layout.yesno);
        yes = (Button) Reset.findViewById(R.id.YES);
        no = (Button) Reset.findViewById(R.id.NO);
        yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        reset();
                        Reset.dismiss();
                        Intent intent = new Intent(MainActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                }
        });
        no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Reset.dismiss();
                }
        });
        Reset.show();
        }

    private void openActivity2() {
        Intent intent = new Intent(this,Activity2.class);
        startActivity(intent);
    }

    private void displayToast(String message) {
        String winner = "Pobjedili su " + message;
        Toast.makeText(this, winner, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {
    }
}






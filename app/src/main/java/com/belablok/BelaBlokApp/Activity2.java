package com.belablok.BelaBlokApp;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


public class Activity2 extends AppCompatActivity implements View.OnClickListener{
    private Button input;
    private EditText callingsYou;
    private EditText callingsWe;
    private EditText pointsWe;
    private EditText pointsYou;
    private int wePoints;
    private int youPoints;
    Window window;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity2);
        if(Build.VERSION.SDK_INT >=23 ){
            window=this.getWindow();
            window.setStatusBarColor(this.getResources().getColor(R.color.black));
        }
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        inicalize();
        boolean update = getIntent().getBooleanExtra("EXTRA_UPDATE", false);
        int position = getIntent().getIntExtra("EXTRA_position", 0);
        if (update){
            setup(position);
        }
        SetOnClickListeners(update,position);
        AddTextChangeListenders();
    }

    private void SetOnClickListeners(boolean update,int position) {
        input.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity1(update);
            }
        });
    }

    private void AddTextChangeListenders() {
        pointsWe.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (pointsWe.hasFocus()) {
                    Integer PointsWe;
                    if (pointsWe.getText().toString().equals("")) {
                        PointsWe = 0;
                        pointsWe.setText(String.valueOf(0));
                    }

                    else {
                        PointsWe = Integer.parseInt(pointsWe.getText().toString());
                    }
                    int a = 162 - PointsWe;
                    pointsYou.setText(String.valueOf(a));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                Integer PointsWe = Integer.parseInt(pointsWe.getText().toString());
                if(PointsWe >162){
                    pointsWe.setText(String.valueOf(162));
                }
            }
        });
        pointsYou.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (pointsYou.hasFocus()) {
                    Integer PointsYou;
                    if (pointsYou.getText().toString().equals("")) {
                        PointsYou = 0;
                        pointsYou.setText(String.valueOf(0));
                    } else {
                        PointsYou = Integer.parseInt(pointsYou.getText().toString());
                    }
                    int a = 162 - PointsYou;
                    pointsWe.setText(String.valueOf(a));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                Integer PointsYou = Integer.parseInt(pointsYou.getText().toString());
                if(PointsYou >162){
                    pointsYou.setText(String.valueOf(162));
                }
            }
        });
    }


    private void inicalize(){
        this.input = (Button) findViewById(R.id.input);
        this.pointsWe = (EditText) findViewById(R.id.pointsWe);
        this.pointsYou = (EditText) findViewById(R.id.pointsYou);
        this.callingsYou = (EditText) findViewById(R.id.callingsYou);
        this.callingsWe = (EditText) findViewById(R.id.callingsWe);
    }

    private void setup(int position) {
        pointsWe.setText(Integer.toString(GlobalResults.wePoints.get(position)), TextView.BufferType.EDITABLE);
        pointsYou.setText(Integer.toString(GlobalResults.youPoints.get(position)), TextView.BufferType.EDITABLE);
        callingsWe.setText(Integer.toString(GlobalResults.wecallings.get(position)), TextView.BufferType.EDITABLE);
        callingsYou.setText(Integer.toString(GlobalResults.youcallings.get(position)), TextView.BufferType.EDITABLE);
        }


    private void openActivity1(boolean update) {
        if (update) {
            getPoints();
            activate("update");
        }
        else {
            getPoints();
            if (GlobalResults.names.isEmpty() != true && !(GlobalResults.names.get(1).equals(""))) {
                displayToast();
            }
            if (GlobalResults.shuffle == 3) {
                GlobalResults.shuffle = 0;
            } else {
                GlobalResults.shuffle += 1;
            }
            activate("input");
        }
    }

    private void getPoints() {
        int points1;
        int points2;
        int points3;
        int points4;
        if (pointsWe.getText().toString().equals("")) {
            points1 = 0;
        } else {
            points1 = Integer.parseInt(pointsWe.getText().toString());
        }
        if (pointsYou.getText().toString().equals("")) {
            points2 = 0;
        } else {
            points2 = Integer.parseInt(pointsYou.getText().toString());
        }
        if (callingsWe.getText().toString().equals("")) {
            points3 = 0;
        } else {
            points3 = Integer.parseInt(callingsWe.getText().toString());
        }
        if (callingsYou.getText().toString().equals("")) {
            points4 = 0;
        } else {
            points4 = Integer.parseInt(callingsYou.getText().toString());
        }
        GlobalResults.wePoints.add(points1);
        GlobalResults.youPoints.add(points2);
        GlobalResults.wecallings.add(points3);
        GlobalResults.youcallings.add(points4);
        wePoints = points1 + points3;
        youPoints = points2 + points4;
    }

    private void displayToast(){
        Integer a = GlobalResults.shuffle;
        String shuffles = "Mješa " + GlobalResults.names.get(a);
        Toast.makeText(this,shuffles,Toast.LENGTH_LONG).show();
    }


    private void activate(String act){
        Intent intent = new Intent(this, MainActivity.class);

        if(act.equals("input")) {
            intent.putExtra("EXTRA_POINTSWE", wePoints);
            intent.putExtra("EXTRA_POINTSYOU", youPoints);
            intent.putExtra("EXTRA_INPUTYN", true);

        }
        else {
            int position =getIntent().getIntExtra("EXTRA_position",0);
            intent.putExtra("EXTRA_POINTSWE_CHANGE", wePoints);
            intent.putExtra("EXTRA_POINTSYOU_CHANGE", youPoints);
            intent.putExtra("EXTRA_POSITION", position);
            intent.putExtra("EXTRA_INPUTYN_CHANGE", true);

        }
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {
    }
};



package com.belablok.BelaBlokApp;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Activity3_1 extends AppCompatActivity implements View.OnClickListener {
    Window window;
    private Button confirm;
    private Button clear;
    private EditText player1;
    private EditText player2;
    private EditText player3;
    private EditText player4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity3_2);
        setTitle("Bela Blok");
        inicalize();
        setOnClickListeners();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if(Build.VERSION.SDK_INT >=23 ){
            window=this.getWindow();
            window.setStatusBarColor(this.getResources().getColor(R.color.black));
        }
        if(GlobalResults.names.isEmpty() == false){
            setNames();
        }
    }

    private void setOnClickListeners() {
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GlobalResults.names.clear();
                Activity1();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity1();
            }
        });
    }

    private void setNames() {
        player1.setText(GlobalResults.names.get(0),TextView.BufferType.EDITABLE);
        player2.setText(GlobalResults.names.get(1),TextView.BufferType.EDITABLE);
        player3.setText(GlobalResults.names.get(2),TextView.BufferType.EDITABLE);
        player4.setText(GlobalResults.names.get(3),TextView.BufferType.EDITABLE);
    }

    private void inicalize() {
        this.confirm = (Button) findViewById(R.id.confirm);
        this.player1 = (EditText) findViewById(R.id.name1);
        this.player2 = (EditText) findViewById(R.id.name2);
        this.player3 = (EditText) findViewById(R.id.name3);
        this.player4 = (EditText) findViewById(R.id.name4);
        this.clear = (Button) findViewById(R.id.clear);
    }


    public void onClick(View v) {
        openActivity1();
    }

    private void openActivity1() {
        GlobalResults.names.add(0,player1.getText().toString());
        GlobalResults.names.add(1,player2.getText().toString());
        GlobalResults.names.add(2,player3.getText().toString());
        GlobalResults.names.add(3,player4.getText().toString());
        Activity1();
    }
    private void Activity1(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
